#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse


def get_basic_parser(has_single_csv=False):
    """Return the default options used in several scripts"""
    parser = argparse.ArgumentParser()
    if has_single_csv:
        parser.add_argument('csv_file', type=str, help="CSV file containing the code bench statistics")
    else:
        parser.add_argument('csv_files', type=str, nargs='+', help="CSV files containing the codec bench statistics")
    parser.add_argument('-n', '--no-header', action='store_true', default=False, help="If CSV files have no header")
    return parser


