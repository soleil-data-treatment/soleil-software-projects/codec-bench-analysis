#!/usr/bin/env python
# -*- coding: utf-8 -*-

INCHES_PER_PT = 1 / 72.27
GOLDEN_RATIO = 1.618033988

DEFAULT_WIDTH = 307.2898
DEFAULT_FRACTION = 3.


def set_size(width=DEFAULT_WIDTH, fraction=DEFAULT_FRACTION):
    """Set figure dimensions to avoid scaling in LaTeX"""
    # Width of figure (in pts)
    fig_width_pt = width * fraction
    fig_width_in = fig_width_pt * INCHES_PER_PT
    fig_height_in = fig_width_in / GOLDEN_RATIO
    return fig_width_in, fig_height_in


if __name__ == '__main__':
    width = 307  # found using \showthe\textwidth in the beamer latex file
    dims = set_size(width)
    print(dims)