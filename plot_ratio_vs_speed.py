#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse 

import matplotlib.pyplot as plt
import numpy as np

import settings
from metrics import DEFAULT_REF_SPEED
from metrics import CodecObservables
from metrics import get_theta_r_inverse
from metrics import get_theta_t_inverse
from metrics import get_theta_w_inverse
from options import get_basic_parser
from read_csv import get_compressor_info
from merge_csv import get_statistics
from merge_csv import has_same_datafile

DEFAULT_XAXIS = 'Vc'
XAXIS_CHOICES = ['Vc', 'Vd', 'theta_r_inv', 'theta_t_inv', 'theta_w_inv']


class UnknownXaxis(Exception):
    pass


def get_compressor_names(compressors):
    """Return the compressor full names from a list of compressors"""
    compressor_names = []
    for comp in compressors:
        c = get_compressor_info(comp)
        # name = "{} {}".format(c.name, c.options) if c.options else c.name
        name = c.name
        compressor_names.append(name)
    return compressor_names


def plot_comp_ratio_vs_comp_speed(statistics, vref=DEFAULT_REF_SPEED, xaxis=DEFAULT_XAXIS):
    """Plot the compression ratio vs the compression speed as found in the lzbench csv file"""
    for stat in statistics:
        r, vc, vd = stat['R'], stat['Vc'], stat['Vd']
        obs = CodecObservables(r, vc, vd, vref=vref)
        # stat['theta_r_inv'] = get_theta_r_inverse(obs)
        stat['theta_t_inv'] = get_theta_t_inverse(obs)
        # stat['theta_w_inv'] = get_theta_w_inverse(obs)

    if xaxis == 'Vc':
        x = np.array([s['Vc'] for s in statistics])  # compression speed
        xlabel = r'$V_c$ (MB/s)'
    elif xaxis == 'Vd':
        x = np.array([s['Vd'] for s in statistics])  # decompression speed
        xlabel = r'$V_d$ (MB/s)'
    elif xaxis == 'theta_t_inv':
        x = x = np.array([s['theta_t_inv'] for s in statistics])
        xlabel = r'$\theta_t^{-1}$ [assuming $V_t=$' + '{} MB/s]'.format(vref)
    else:
        raise UnknownXaxis

    # In lzbench, the compression ratio is incorrectly defined (it should be N/Nc which is > 1)
    # So it is better to recompute it explicitly
    uncompressed_size = np.array([s['N'] for s in statistics])
    compressed_size = np.array([s['Nc'] for s in statistics])
    y = uncompressed_size / compressed_size  # compression ratio
    categories = np.array([s['csv_file_id'] for s in statistics])
    num = categories.max()

    datafile_ids = np.array([s['datafile_id'] for s in statistics])
    compressors = [s['compressor'] for s in statistics]
    compressor_names = get_compressor_names(compressors)

    # The legend should be more explicit when the datafiles are different
    same_datafile = has_same_datafile(statistics)
    if not same_datafile:
        compressor_names = ['{} (file #{})'.format(s1, str(s2)) for s1, s2 in zip(compressor_names, datafile_ids)]

    labels = list(dict.fromkeys(compressor_names))  # Remove duplicates while preserving order

    fig, ax = plt.subplots(figsize=(10,10))
    c = None if num == 0 else categories
    scatter = ax.scatter(x, y, marker='o', c=c, cmap='tab20')

    if num > 0:
        handles, _ = scatter.legend_elements(num=num)
        legend = ax.legend(handles, labels, loc="upper right", title="Compressors")
        ax.add_artist(legend)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(r'$R$')
    ax.set_xscale('log')  # Vc range can be quite large

    for i, txt in enumerate(compressors):
        ax.annotate(txt, (x[i], y[i]), xytext=(10,10), textcoords='offset points', rotation=45)

    plt.show()


if __name__ == '__main__':

    parser = get_basic_parser()
    parser.add_argument('-vref', '--ref-speed', type=float, default=DEFAULT_REF_SPEED,
                        help="Reference speed (MB/s) [e.g., transfer, write, read]")
    parser.add_argument('-x', '--xaxis', type=str, default=DEFAULT_XAXIS,
                        choices=['Vc', 'Vd', 'theta_r_inv', 'theta_t_inv', 'theta_w_inv'],
                        help="The x xaxis that will be plotted against the compression ratio")
    args = parser.parse_args()

    has_header = not args.no_header
    results = get_statistics(args.csv_files, has_header=has_header)
    plot_comp_ratio_vs_comp_speed(results, vref=args.ref_speed, xaxis=args.xaxis)

