#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from os.path import basename

from options import get_basic_parser
from read_csv import CUSTOMED_FIELDNAMES
from read_csv import read_csv


def get_statistics(csv_files, has_header=True):
    """
    Get the statistics from all lzbench CSV files.
    Two fields are added: 'csv_file_id' and 'datafile_id' (file names can be too long to be read on a pot for instance)
    """
    all_results = []
    datafiles = []
    for index, csv_file in enumerate(csv_files):
        results = read_csv(csv_file, fieldnames=CUSTOMED_FIELDNAMES, has_header=has_header)
        if not results: continue
        # Assumes that the datafile is the same for all lines of a given csv file.
        datafile = results[0]['datafile']
        if datafile not in datafiles:
            datafiles.append(datafile)
        for res in results:
            res['csv_file_id'] = index
            res['csv_file_name'] = basename(csv_file)
            res['datafile_id'] = datafiles.index(datafile)
        all_results.extend(results)
    return all_results


def has_same_datafile(all_results):
    """Test if the field 'File' or 'datafile' indicated in the lzbench output CSV files is the same"""
    datafiles = [x['datafile'] for x in all_results]
    same = all(x == datafiles[0] for x in datafiles)
    return same


if __name__ == '__main__':
    parser = get_basic_parser()
    args = parser.parse_args()

    all_results = get_statistics(args.csv_files)
    for res in all_results:
        print(res)

    same = has_same_datafile(all_results)
    print("Same input datafile used?: {}".format(same))