#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

import settings
from beamer import set_size
from options import get_basic_parser
from merge_csv import get_statistics
from read_csv import get_compressor_info


def plot_speeds_vs_threads(statistics):
    """Plot the histogram of values"""
    threads = []
    for s in statistics:
        res = get_compressor_info(s['compressor'])
        thread = int(res.options[2:])  # 'zstd 1.5.0 -1 -T2' -> options is '-T2' and then thread is 2
        threads.append(thread)

    vc = np.array([s['Vc'] for s in statistics])  # compression speed
    vd = np.array([s['Vd'] for s in statistics])  # decompression speed

    fig, ax1 = plt.subplots(figsize=set_size())

    color = 'tab:blue'
    ax1.set_xlabel('Number of threads')
    ax1.set_ylabel(r'$V_c$ (MB/s)', color=color)
    ax1.plot(threads, vc, color=color, marker='o', linestyle='')
    ax1.tick_params(axis='x')
    ax1.tick_params(axis='y', labelcolor=color)

    ax2 = ax1.twinx()

    color = 'tab:red'
    ax2.set_ylabel(r'$V_d$ (MB/s)', color=color)
    ax2.plot(threads, vd, color=color, marker='o', linestyle='')
    ax2.tick_params(axis='y', labelcolor=color)

    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    parser = get_basic_parser()
    args = parser.parse_args()

    results = get_statistics(args.csv_files)
    plot_speeds_vs_threads(results)

