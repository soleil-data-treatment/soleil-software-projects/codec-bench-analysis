# Codec Bench Analysis

Python scripts to analyze the CSV files obtained with the codec benchmark scripts.

# Usage
Compressors can be compared in the compression ratio/speed plane:
```
python3 plot_ratio_vs_speed.py results-1.csv results-2.csv [...]
```

If the compressor was applied several times on the same data set in order to estimate the metric errors, 
one can plot the de\compression speed histograms:
```
python3 plot_speed_histograms.py results.csv
```

If threads were used, the de\compression speeds vs the number of threads can be plotted using:
```
python3 plot_speed_vs_thread.py results.csv
```

Metrics can be printed as follows:
```
python3 print_metrics.py results.csv
```
and sorted either by the compressor full name `compressor`, the compression ratio `R`, 
the metric`theta`, the compression speed `Vc`, or the decompression speed `Vd`:
```
python3 print_metrics.py results.csv -s theta
```

