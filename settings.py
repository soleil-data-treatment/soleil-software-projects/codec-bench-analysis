#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt

FONTSIZE = 14
plt.rc('xtick', labelsize=FONTSIZE)
plt.rc('ytick', labelsize=FONTSIZE)
plt.rc('axes', labelsize=FONTSIZE)

