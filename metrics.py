#!/usr/bin/env python
# -*- coding: utf-8 -*-

DEFAULT_REF_SPEED = 100  # in MB/s -- It could be the transfer, write or read speed


class CodecObservables:
    """Metric class where we store the raw quantities related to data compression"""

    def __init__(self, r, vc, vd, vref=DEFAULT_REF_SPEED):
        self.r = r  # compression ratio
        self.vc = vc  # compression speed
        self.vd = vd  # decompression speed
        self.vref = vref  # reference speed (i.e., transfer/read/write speed)

    @property
    def alpha(self):
        """Return α = Vd / Vc"""
        return self.vd / self.vc

    @property
    def sigma_c(self):
        """Return σ_c = Vc / Vref"""
        return self.vc / self.vref

    @property
    def sigma_d(self):
        """Return σ_d = Vd / Vref"""
        return self.vd / self.vref


METRICS = []


def metric(metric_func):
    METRICS.append(metric_func)
    return metric_func


@metric
def get_theta_t_inverse(obs):
    """Metric in the case of a transfer over the network. Compute θ_t = 1/σ_c + 1/R + 1/σ_d"""
    theta_t = 1 / obs.sigma_c + 1 / obs.r + 1 / obs.sigma_d
    return 1 / theta_t


@metric
def get_theta_w_inverse(obs):
    """Metric in the case of a compress-and-write operation. Compute θ_w = 1/σ_c + 1/R"""
    theta_w = 1 / obs.sigma_c + 1 / obs.r
    return 1 / theta_w


@metric
def get_theta_r_inverse(obs):
    """Metric in the case of a read-and-decompress operation. Compute θ_r = 1/R + 1/σ_d"""
    theta_r = 1 / obs.r + 1 / obs.sigma_d
    return 1 / theta_r


if __name__ == '__main__':
    r = 5
    vc, vd, vref = 700, 1000, 100
    obs = CodecObservables(r, vc, vd, vref=vref)
    for m in METRICS:
        print(m(obs))

