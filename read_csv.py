#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import csv
from collections import namedtuple

from options import get_basic_parser

CUSTOMED_FIELDNAMES = ['compressor', 'Vc', 'Vd', 'N', 'Nc', 'R', 'datafile']

Compressor = namedtuple('Compressor', ['name', 'version', 'level', 'options'])


def read_csv(csv_file, fieldnames=None, has_header=True):
    """Read the csv file created by lzbench"""
    results = []
    with open(csv_file) as f:
        reader = csv.DictReader(f, fieldnames=fieldnames, delimiter=',')
        if has_header:
            try:
                next(reader)  # skip the header
            except StopIteration:
                print("Skipping file: {}".format(csv_file))
        for row in reader:
            if row['compressor'].strip() == 'memcpy':
                continue
            formatted_row = {}
            for key, value in row.items():
                try:
                    formatted_row[key] = float(value)
                except ValueError:
                    formatted_row[key] = value
            results.append(formatted_row)
    return results


def get_compressor_info(compressor):
    """
    Get compressor name, version and compression level
    For example, if compressor = 'zstd 1.5.0 -1', then name = 'zstd', version = '1.5.0' and level = '1'
    There is a last field 'options' where one can specify other options passed to the compressor (e.g., # of threads)
    """
    level = None
    options = None

    values = compressor.split()
    try:
        name = values[0]
        version = values[1]
    except IndexError:
        raise Exception("Looks like a weird compressor: {}".format(compressor))

    try:
        level = values[2]
        remaining_values = values[3:]
        if remaining_values: options = " ".join(remaining_values)
    except IndexError:
        pass

    try:
        level = int(level[1:])
    except TypeError:
        pass
    return Compressor(name, version, level, options)


if __name__ == '__main__':
    parser = get_basic_parser(has_single_csv=True)
    args = parser.parse_args()

    has_header = not args.no_header
    results = read_csv(args.csv_file, fieldnames=CUSTOMED_FIELDNAMES, has_header=has_header)
    for res in results:
        print(res)
