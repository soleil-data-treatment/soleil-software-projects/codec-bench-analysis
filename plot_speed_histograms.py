#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

import matplotlib.pyplot as plt
import numpy as np

import settings
from beamer import set_size
from options import get_basic_parser
from merge_csv import get_statistics

DECIMALS = -1  # rounds one position to the left of the decimal point
BIN_SIZE = 10**(-DECIMALS)


def plot_compression_speed_histograms(statistics):
    """Plot the histogram of values"""
    csv_files = sorted(list(set([(s['csv_file_id'], s['csv_file_name'], s['compressor']) for s in statistics])))

    nb_plots = len(csv_files)
    fig, axs = plt.subplots(nb_plots, sharex=True, sharey=True, figsize=set_size())
    if nb_plots == 1: axs = [axs]

    vcs = np.array([s['Vc'] for s in statistics])
    vc_min, vc_max = vcs.min().round(DECIMALS), vcs.max().round(DECIMALS)
    bins = np.arange(vc_min, vc_max, BIN_SIZE)

    for i, csv_file_name, compressor in csv_files:
        vc = np.array([s['Vc'] for s in statistics if s['csv_file_id'] == i])  # compression speed
        c_n, c_bins, c_patches = axs[i].hist(vc, bins=bins, density=False, alpha=0.7, label=compressor)
        axs[i].legend(loc=1)
        if i == nb_plots-1:
            axs[i].set_xlabel(r'$V_c$ (MB/s)')
    plt.subplots_adjust(hspace=.0)
    plt.show()


def plot_speeds_histogram(statistics, same_speed_range=False):
    """Plot the histogram of values"""
    csv_files = sorted(list(set([(s['csv_file_id'], s['csv_file_name'], s['compressor']) for s in statistics])))

    fig, (ax1, ax2) = plt.subplots(2, sharex=same_speed_range, sharey=True, figsize=set_size())

    for i, csv_file_name, compressor in csv_files:
        vcs = np.array([s['Vc'] for s in statistics if s['csv_file_id'] == i])  # compression speed
        # vc_min, vc_max = vcs.min().round(DECIMALS), vcs.max().round(DECIMALS)
        # bins = np.arange(vc_min, vc_max, BIN_SIZE)
        bins = 100
        c_n, c_bins, c_patches = ax1.hist(vcs, bins, density=False, alpha=0.7, label=compressor)
        ax1.set_xlabel(r'$V_c$ (MB/s)')
        ax1.legend(loc=1)

        vds = np.array([s['Vd'] for s in statistics if s['csv_file_id'] == i])  # decompression speed
        # vd_min, vd_max = vds.min().round(DECIMALS), vds.max().round(DECIMALS)
        # bins = np.arange(vd_min, vd_max, BIN_SIZE)
        d_n, d_bins, d_patches = ax2.hist(vds, bins, density=False, alpha=0.7, label=compressor)
        ax2.set_xlabel(r'$V_d$ (MB/s)')
        ax2.legend(loc=1)
    ax1.xaxis.set_tick_params(which='both', labelbottom=True)
    plt.show()


if __name__ == '__main__':
    parser = get_basic_parser()
    parser.add_argument('-s', '--same-speed-range', action='store_true', default=False,
                        help="Plot compression and decompression speeds within the same limits")
    args = parser.parse_args()

    has_header = not args.no_header
    results = get_statistics(args.csv_files, has_header=has_header)
    plot_compression_speed_histograms(results)
    plot_speeds_histogram(results, same_speed_range=args.same_speed_range)
