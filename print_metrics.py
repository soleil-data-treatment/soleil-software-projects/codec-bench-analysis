#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from operator import itemgetter

import matplotlib.pyplot as plt
import numpy as np
from tabulate import tabulate

from metrics import DEFAULT_REF_SPEED
from metrics import CodecObservables
from metrics import get_theta_t_inverse
from merge_csv import get_statistics
from merge_csv import has_same_datafile

DEFAULT_TABLE_FMT = 'pipe'


def print_metrics(statistics, vref=DEFAULT_REF_SPEED, tablefmt=DEFAULT_TABLE_FMT, sort_by=None):
    """For each compressor, print R, Vc, Vd, α, θ"""
    same_datafile = has_same_datafile(statistics)
    floatfmt = (".0f", ".1f", ".0f", ".0f", ".1f", ".2f",)
    results = []
    for stat in statistics:
        r = stat['N'] / stat['Nc']  # recompute the compression ratio (so that our definition is always consistent)
        obs = CodecObservables(r, stat['Vc'], stat['Vd'], vref=vref)
        res = {
            'compressor': stat['compressor'],
            'R': obs.r,
            'Vc': obs.vc,
            'Vd': obs.vd,
            'alpha': obs.alpha,
            'theta_inv': get_theta_t_inverse(obs),
        }
        if not same_datafile:
            res['datafile'] = stat['datafile']
        results.append(res)
    if sort_by:
        reverse = False if (sort_by == 'compressor') else True
        results.sort(key=itemgetter(sort_by), reverse=reverse)
    print("Results for Vref = {} MB/s".format(vref))
    print(tabulate(results, headers='keys', tablefmt=tablefmt, floatfmt=floatfmt))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('csv_files', type=str, nargs='+', help="CSV files containing the lzbench statistics")
    parser.add_argument('-vref', '--ref-speed', type=float, default=DEFAULT_REF_SPEED,
                        help="Reference speed (MB/s) [e.g., transfer, write, read]")
    parser.add_argument('-s', '--sort-by', type=str, default=None, choices=['compressor', 'R', 'theta_inv', 'Vc', 'Vd'],
                        help="Sort the output table along this axis")
    parser.add_argument('-f', '--tablefmt', type=str, default=DEFAULT_TABLE_FMT,
                        choices=['pipe', 'latex', 'simple', 'pretty'], help="Defines how the output table is formatted")
    args = parser.parse_args()

    results = get_statistics(args.csv_files)
    print_metrics(results, vref=args.ref_speed, tablefmt=args.tablefmt, sort_by=args.sort_by)
